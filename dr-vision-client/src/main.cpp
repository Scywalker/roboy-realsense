/*
 * main.cc
 *
 *  Created on: 12.12.2015
 *  Author: Dario Rethage
 */

#include "DRVisionClient.h"

int main(int argc, char** argv) {

	std::string host = "localhost";
	std::string port = "9090";

	DRVisionClientPtr client = DRVisionClientPtr(new DRVisionClient(host,port));

	client->connect();

	//Create list of subscribing topics
	std::vector<std::string> subscribe_topics;
	subscribe_topics.push_back("/roboy_state");
	subscribe_topics.push_back("/roboy_vision_node");

	//Set subscribing topics
	client->subscribeToTopics(subscribe_topics, 1000);

	//Publish a std_msgs message to topic
	client->publishStdMsgsString("/face_location", "100,100");

	//Listen for msgs on subscribed topics
	client->spin();
	while(true) {
		for(std::vector<std::string>::iterator it = subscribe_topics.begin(); it != subscribe_topics.end(); ++it) {
			client->log(*it, client->readLastValue(*it)->toStyledString());
		}

		client->wait();
	}
}



