/*
 * DRClient.cpp
 *
 *  Created on: Dec 15, 2015
 *      Author: Dario Rethage
 */

#include "DRVisionClient.h"

DRVisionClient::DRVisionClient(std::string host, std::string port)
: rosbridge::RosbridgeClient(host, port) {


}

void DRVisionClient::publishStdMsgsString(const std::string& topic, const std::string& msg) {

	Json::Value root;
	root["data"] = Json::Value(msg);
	publish(topic, "std_msgs/String", root);
}

void DRVisionClient::subscribeToTopics(std::vector<std::string> topics, unsigned int buffer_size) {

	for(std::vector<std::string>::iterator it = topics.begin(); it != topics.end(); ++it) {
	    subscribe(*it, buffer_size);
	}
}

void DRVisionClient::log(std::string tag, std::string s) {
	std::cout << "[DRVisionClient] " << tag << ": " << s << std::endl;
}

void DRVisionClient::log(std::string s) {
	std::cout << "[DRVisionClient] " << s << std::endl;
}

DRVisionClient::~DRVisionClient() {
	// TODO Auto-generated destructor stub
}

