/*
 * DRClient.h
 *
 *  Created on: Dec 15, 2015
 *      Author: Dario Rethage
 */

#ifndef SRC_DRVisionCLIENT_H_
#define SRC_DRVisionCLIENT_H_

#include "rosbridgeclient/RosbridgeClient.h"
#include <vector>

class DRVisionClient: public rosbridge::RosbridgeClient {
public:
	DRVisionClient(std::string host, std::string port);
	void subscribeToTopics(std::vector<std::string> topics, unsigned int buffer_size = 1000);
	void publishStdMsgsString(const std::string& topic, const std::string& msg);
	void log(std::string tag, std::string s);
	void log(std::string s);
	virtual ~DRVisionClient();
};

typedef boost::shared_ptr<DRVisionClient> DRVisionClientPtr;

#endif /* SRC_DRVisionCLIENT_H_ */
